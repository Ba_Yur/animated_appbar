import 'package:animated_appbar/customAppBar.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  AnimationController _animationController;

  Animation<double> _height;
  Animation<double> _shadow;
  bool isTapped = false;


  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(
        milliseconds: 1000,
      ),
      reverseDuration: Duration(milliseconds: 1000),
    );
    _height = Tween<double>(begin: 100, end: 300).animate(_animationController);
    _shadow = Tween<double>(begin: 0, end: 40).animate(_animationController);

    _animationController.addListener(() {
      setState(() {});
    });

    _animationController.addStatusListener(
      (status) {
        if (status == AnimationStatus.completed) {
          setState(() {
            isTapped = !isTapped;
          });
          _animationController.forward();
        }
        if (status == AnimationStatus.dismissed) {
          setState(() {
            isTapped = !isTapped;
          });
        }
      },
    );

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(_animationController, 'Some Title', _height.value, _shadow.value),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            isTapped
                ? _animationController.reverse()
                : _animationController.forward();
            print(_height);
          },
          child: Text('Tap me'),
        ),
      ),
    );
  }
}
