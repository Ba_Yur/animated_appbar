import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget with PreferredSizeWidget {
  AnimationController controller;
  String appBarTitle;
  double appBarHeight;
  double appBarShadow;

  CustomAppBar(
      this.controller, this.appBarTitle, this.appBarHeight, this.appBarShadow);

  @override
  Widget build(BuildContext context) {
    return PreferredSize(
        child: AnimatedBuilder(
          animation: controller,
          builder: (BuildContext context, Widget _) {
            return Container(
              height: appBarHeight,
              child: Center(
                child: Text(appBarTitle),
              ),
              decoration: BoxDecoration(color: Colors.green, boxShadow: [
                BoxShadow(color: Colors.green, blurRadius: appBarShadow)
              ]),
            );
          },
        ),

        preferredSize: Size.fromHeight(appBarHeight));
  }
  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(appBarHeight);
}
